import operator
import os
from decimal import ROUND_HALF_EVEN
import moneyed
from moneyed import CURRENCIES, DEFAULT_CURRENCY_CODE
from moneyed.localization import _FORMATTER, DEFAULT

from LavoMas.settings import production

BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework_datatables',
    'App',
    'pwa',
    'django_filters',
    'widget_tweaks',
    'crispy_forms',
    'djmoney',
    'djmoney.contrib.exchange',
    'social_django'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',

]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.google.GoogleOAuth2',
    'social_core.backends.google.GoogleOAuth',
)

ROOT_URLCONF = 'LavoMas.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',

            ],
        },
    },
]

WSGI_APPLICATION = 'LavoMas.wsgi.application'

# Internationalization
LANGUAGE_CODE = 'es-CL'
TIME_ZONE = 'America/Santiago'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = ''
EMAIL_HOST_USER = ''
DEFAULT_FROM_EMAIL = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 465
EMAIL_USE_TLS = False
EMAIL_USE_SSL = True

ENABLE_USER_ACTIVATION = False
DISABLE_USERNAME = False
LOGIN_VIA_EMAIL = False
LOGIN_VIA_EMAIL_OR_USERNAME = True
LOGIN_REDIRECT_URL = 'index'
LOGIN_URL = 'App:log_in'
USE_REMEMBER_ME = False

RESTORE_PASSWORD_VIA_EMAIL_OR_USERNAME = True
EMAIL_ACTIVATION_AFTER_CHANGING = True

MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'

SIGN_UP_FIELDS = ['first_name',
                  'last_name', 'email', 'password1', 'password2']

if DISABLE_USERNAME:
    SIGN_UP_FIELDS = ['first_name', 'last_name',
                      'email', 'password1', 'password2']

CRISPY_TEMPLATE_PACK = 'bootstrap4'

# The default currency choices, you can define this in your project's
# settings module
PROJECT_CURRENCIES = getattr(production, "CURRENCIES", None)
CURRENCY_CHOICES = getattr(production, "CURRENCY_CHOICES", None)

if CURRENCY_CHOICES is None:
    if PROJECT_CURRENCIES:
        CURRENCY_CHOICES = [(code, CURRENCIES[code].name) for code in PROJECT_CURRENCIES]
    else:
        CURRENCY_CHOICES = [(c.code, c.name) for i, c in CURRENCIES.items() if c.code != DEFAULT_CURRENCY_CODE]

CURRENCY_CHOICES.sort(key=operator.itemgetter(1, 0))
DECIMAL_PLACES = getattr(production, "CURRENCY_DECIMAL_PLACES", 0)
DECIMAL_PLACES_DISPLAY = getattr(
    production, "CURRENCY_DECIMAL_PLACES_DISPLAY", {currency[0]: DECIMAL_PLACES for currency in CURRENCY_CHOICES}
)

_FORMATTER.add_sign_definition('es_CL', moneyed.CLP, prefix='$')
_FORMATTER.add_formatting_definition(
    'es_CL', group_size=3, group_separator='.', decimal_point=',',
    positive_sign='', trailing_positive_sign='',
    negative_sign='-', trailing_negative_sign='',
    rounding_method=ROUND_HALF_EVEN
)

# DJANGO SOCIAL AUTH
LOGIN_URL = 'log_in'
LOGOUT_URL = 'log_out'
LOGIN_REDIRECT_URL = 'index'

SOCIAL_AUTH_FACEBOOK_KEY = '277458657427858'  # App ID
SOCIAL_AUTH_FACEBOOK_SECRET = 'bad3213e9327248baafcc766f74f4df2'  # App Secret

SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
