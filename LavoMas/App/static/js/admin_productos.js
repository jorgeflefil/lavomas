
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (input.id === "imagen") {
                $('#imagen_producto_muestra_crear').attr('src', e.target.result);
            } else {
                $('#imagen_producto_muestra').attr('src', e.target.result);
            }

        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagen").change(function () {
    readURL(this);
});

$("#modificar_imagen").change(function () {
    readURL(this);
});

function LimpiarFormulario() {
    $('#descripcion_producto').val("")
    $('#precio').val("")
    $('#stock').val("")
    $('#imagen').val("")
    $('#imagen_producto_muestra_crear').attr('src', "/media/no-disponible.png");
}


function cargar_producto(id_producto) {
    $("#id_producto").val(id_producto)
    $.ajax({
        url: "api/productos/" + id_producto,
        type: 'GET',
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function (data) {
            if (data.imagen_producto) {
                $("#imagen_producto_muestra").attr("src", data.imagen_producto)
            } else {
                $("#imagen_producto_muestra").attr("src", "/media/no-disponible.png")
            }
            $("#modificar_id_producto").val(data.pk)
            $("#modificar_titulo_producto").val(data.titulo_producto)
            $("#modificar_descripcion_producto").val(data.descripcion_producto)
            $("#modificar_precio").val(data.precio)
            $("#modificar_precio_descuento").val(data.precio_descuento)
            $("#modificar_stock").val(data.stock)
            $('#modificar_categoria option[value=' + data.categoria + ']').attr('selected', 'selected');
            $('#modificar_label option[value=' + data.label + ']').attr('selected', 'selected');
        }
    })
}

$("#form_agrega_producto").submit(function (event) {
    event.preventDefault();

    $('#modal_agregar_producto').modal('hide');

    var formData = new FormData($(this)[0]);

    var settings = {
        url: "/api/productos/",
        method: "POST",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function (data) {

        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    $.ajax(settings).done(function (response) {
        var accion = "crear";

        if ($('#imagen').get(0).files.length === 0) {
            console.log("Producto sin imagen creado correctamente");
            window.location = 'productos';
        } else {
            SubirImagen(response, accion);
        }
        LimpiarFormulario();
    });
});

$("#form_modificar_producto").submit(function (event) {
    event.preventDefault();

    $('#modal_modificar_producto').modal('hide');

    var formData = new FormData();
    formData.append('id_producto', $("#modificar_id_producto").val());
    formData.append('titulo_producto', $("#modificar_titulo_producto").val());
    formData.append('descripcion_producto', $("#modificar_descripcion_producto").val());
    formData.append('precio', $("#modificar_precio").val());
    formData.append('precio_descuento', $("#modificar_precio_descuento").val());
    formData.append('stock', $("#modificar_stock").val());
    formData.append('categoria', $("#modificar_categoria").val());
    formData.append('label', $("#modificar_label").val());

    var settings = {
        url: "/api/productos/" + $("#modificar_id_producto").val() + "/",
        method: "PUT",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function (data) {
        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    $.ajax(settings).done(function (response) {
        var accion = "modificar";
        if ($('#modificar_imagen').get(0).files.length === 0) {
            console.log("Producto sin imagen modificado correctamente");
            window.location = 'productos';
        } else {
            SubirImagen($("#modificar_id_producto").val(), accion);
        }
    });
});


function cargar_eliminar_producto(id_producto) {
    $("#id_producto_eliminar").val(id_producto)
}


$("#form_eliminar_producto").submit(function (event) {
    event.preventDefault();
    var settings = {
        url: "api/productos/" + $("#id_producto_eliminar").val() + "/",
        method: "DELETE",
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function (data) {
            $('.btn-close').click();
            window.location = 'productos';
        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
});

$("#form_agrega_categoria").submit(function (event) {
    event.preventDefault();

    var formData = new FormData($(this)[0]);
    console.log(event)
    console.log(formData)

    var settings = {
        url: "/api/categorias/",
        method: "POST",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function (data) {
            $('.btn-close').click()
        },
        failure: function (data) {
            alert('Got an error');
        }
    };

    $.ajax(settings).done(function (response) {
        console.log("Categoria creada correctamente");
        window.location = 'productos';

    });

});

function SubirImagen(pk, accion) {
    var formData = new FormData();
    if (accion == "crear") {
        formData.append('imagen_producto', document.getElementById('imagen').files[0]);
    } else {
        formData.append('imagen_producto', document.getElementById('modificar_imagen').files[0]);
    }
    var settings = {
        url: "/api/productos/" + pk + "/pic/",
        method: "PUT",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function (data) {
            window.location = 'productos';
        },
        failure: function (data) {
            alert('Got an error');
        }
    };
    $.ajax(settings).done(function (response) {
        console.log("Producto creado correctamente");
    });
}

$(document).ready(function () {
    $('#lista_de_productos').DataTable({
        info: false,
        autoWidth: true,
        serverSide: false,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },

    })
});