
$(document).ready(function () {
    let lista = [];
    var total = 0;
    var subtotal = 0;
    var iva = 0;
    var table = $("#lista_de_productos_boleta").DataTable({
        rowReorder: true,
        autoWidth: true,
        ordering: true,
        info: false,
        serverSide: true,
        scrollCollapse: true,
        paging: false,
        select: "single",
        ajax: "/api/productos/?format=datatables",
        columns: [
            {
                data: "imagen_producto",
                searchable: false,
                render: function (data, type, row) {
                    if (data != null) {
                        return '<a href="' + data + '" data-lightbox="image-1" data-title="' + row.descripcion_producto + '"><img style="width:100px" src=' + data + ' /></a>';
                    } else {
                        return '<a href="/media/no-disponible.png" data-lightbox="image-1" data-title="' + row.descripcion_producto + '"><img style="width:100px" src="/media/no-disponible.png" /></a>';
                    }
                },
                className: "columna-imagen",
                width: "10%",
            },
            {
                data: "descripcion_producto",
                searchable: true,
                render: function (data, type, row) {
                    return '<p">' + row.descripcion_producto + '</p>'
                },

            },
            {
                data: "precio",
                searchable: true,
                className: "columna-texto",
                render: function (data, type, row) {
                    return '$ ' + Math.round(data);
                }
            },
            {
                data: "stock",
                searchable: true,
                className: "columna-texto",
            },
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
    }).on("select", function (e, dt, type, indexes) {
        var rowData = table.rows(indexes).data().toArray()
        $('#row-data').html(JSON.stringify(rowData));
    })
    .on("deselect", function () {
            $('#row-data').empty();
    });

    $('#lista_de_productos_boleta tbody').on('click tap', 'tr', function () {
        data = table.row(this).data();
        var id_prod = data.pk;
        var descripcion_producto = data.descripcion_producto;
        var cantidad = 1;
        var precio = parseFloat(data.precio);

        if (lista.findIndex(x => x.id_prod === id_prod) == -1) {
            lista.push({
                id_prod: id_prod,
                descripcion_producto: descripcion_producto,
                cantidad: cantidad,
                precio: precio,
                total: null,
            })
        } else {
            objIndex = lista.findIndex((obj => obj.id_prod == id_prod));
            cantidad = lista[objIndex].cantidad + 1;
            lista[objIndex].cantidad = cantidad;
        }
        $('#collapseExample').collapse()

        CargarTablaVenta(lista)
    });

    $('#tabla_ventas_boleta').on('click', '.btn_borrar', function (event) {
        var producto_seleccionado = $(this).data("value")
        $(this).closest('tr').remove();
        objIndex = lista.findIndex((obj => obj.id_prod == producto_seleccionado));
        lista.splice(objIndex, 1);
        CargarTablaVenta(lista)
    });

    $("#form_confirmar_venta").submit(function (event) {
        event.preventDefault();

        $('#modal_confirmar_venta').modal('hide');
        var formData = new FormData();
        formData.append('user', $("#input_user").val());
        formData.append('subtotal', subtotal);
        formData.append('IVA_venta', iva);
        formData.append('precio_total', total);

        var settings = {
            url: "/api/ventas/",
            method: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            success: function (data) {
                $("#success-alert").show();
                $("#success-alert").alert();
                $("#success-alert").fadeTo(5000, 1000).slideUp(1000, function () {
                    $("#success-alert").slideUp(1000);
                });
            },
            failure: function (data) {
                alert('Got an error');
            }
        };

        $.ajax(settings).done(function (response) {
            SubirVentaDetalle(lista, response);
        });
    });

    function SubirVentaDetalle(lista, id_venta) {

        $.each(lista, function (i, val) {
            var formData = new FormData();
            formData.append('producto', val.id_prod);
            formData.append('venta', id_venta);
            formData.append('precio', val.precio);
            formData.append('cantidad', val.cantidad);
            var settings = {
                url: "/api/detalle_ventas/",
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                beforeSend: function (xhr, settings) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                },
                success: function (data) {
                },
                failure: function (data) {
                    alert('Got an error');
                }
            };
            $.ajax(settings).done(function (response) {
            });
        })

        console.log("Venta realizada")
    }

    $('#btnLimpiarCajaBoleta').on('click', function () {
        lista = [];
        CargarTablaVenta(lista);
    });

    function CargarTablaVenta(lista) {
        $('#tabla_ventas_boleta').find('tbody').detach();
        $('#tabla_ventas_boleta').append($('<tbody>'));

        for (i in lista) {
            $("#tabla_ventas_boleta tbody").append("<tr><td class='my-auto'><button type='button' class='btn btn-danger p-2 btn_borrar' data-value=" + lista[i].id_prod + "><i class='fas fa-trash'></i></button/td><td class='align-middle'>" + lista[i].descripcion_producto + "</td>" +
                "<td class='align-middle table-row'><button class='btn btn-primary p-2 btn_menos_cantidad' data-value=" + lista[i].id_prod + "><i class='fas fa-minus-circle'></i></button><span class='valor_cantidad p-2 m-1 badge badge-warning'>  " + lista[i].cantidad + "</span><button class='btn btn-primary p-2 btn_mas_cantidad' data-value=" + lista[i].id_prod + "><i class='fas fa-plus-circle'></i></button></td>" +
                "<td class='align-middle'>" + lista[i].precio + "</td></tr>");
        }

        $('.btn_mas_cantidad').on('click', function () {
            var producto_seleccionado = $(this).data("value")
            objIndex = lista.findIndex((obj => obj.id_prod == producto_seleccionado));
            cantidad = lista[objIndex].cantidad + 1;
            lista[objIndex].cantidad = cantidad;
            CargarTablaVenta(lista);
        });

        $('.btn_menos_cantidad').on('click', function () {
            var producto_seleccionado = $(this).data("value")
            objIndex = lista.findIndex((obj => obj.id_prod == producto_seleccionado));
            if (lista[objIndex].cantidad > 1) {
                cantidad = lista[objIndex].cantidad - 1;
                lista[objIndex].cantidad = cantidad;
                CargarTablaVenta(lista);
            }
        });
        CargarTotal(lista);
    }

    function CargarTotal(lista) {
        total = 0;
        subtotal = 0;
        iva = 0;
        $.each(lista, function () {
            this.total = this.cantidad * this.precio;
            subtotal = subtotal + this.total;
        });
        iva = subtotal * 0.19;
        total = subtotal + iva;
        $("#subtotal").text("$" + (Math.round(subtotal)));
        $("#total_iva").text("$" + Math.round(iva));
        $("#venta_total").text("$" + Math.round(subtotal + iva));
    }


});