import django_filters
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import View, ListView, DetailView
from django_filters.views import FilterView
from rest_framework import viewsets, response, status, parsers, decorators
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from rest_framework.response import Response
from django.contrib.auth.mixins import LoginRequiredMixin
from ..models import Categoria, Producto, Orden, ProductoOrden
from ..models import LABEL_CHOICES, Address
from ..serializers import ProductoSerializer, ProductoImgSerializer, OrdenSerializer, ProductoOrdenSerializer, \
    UserSerializer, CategoriaSerializer

# ====== FILTROS ======


class ProductFilter(django_filters.FilterSet):
    titulo_producto = django_filters.CharFilter(method='my_custom_filter')

    class Meta:
        model = Producto
        fields = ('titulo_producto', 'categoria')

    def my_custom_filter(self, queryset, name, value):
        return Producto.objects.filter(
            Q(titulo_producto__icontains=value) | Q(descripcion_producto__icontains=value) | Q(categoria__nombre_categoria__icontains=value)
        )


# ====== VIEWSETS ======
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CategoriaViewSet(viewsets.ModelViewSet):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer


class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all().order_by('-imagen_producto')
    serializer_class = ProductoSerializer
    parser_classes = (MultiPartParser, FileUploadParser, FormParser)
    search_fields = ['descripcion_producto']

    filterset_class = ProductFilter

    @decorators.action(
        detail=True,
        methods=['PUT'],
        serializer_class=ProductoImgSerializer,
        parser_classes=[parsers.MultiPartParser],
    )
    def pic(self, request, pk):
        obj = self.get_object()
        print(obj)
        serializer = self.serializer_class(obj, data=request.data,
                                           partial=True)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data)
        return response.Response(serializer.errors,
                                 status.HTTP_400_BAD_REQUEST)

    def create(self, request, pk=None):
        categoria = Categoria.objects.get(pk=request.POST["categoria"])
        producto = Producto.objects.create(
            titulo_producto=request.data["titulo_producto"],
            descripcion_producto=request.data["descripcion_producto"],
            precio=request.data["precio"],
            precio_descuento=request.data["precio_descuento"],
            stock=request.data["stock"],
            categoria=categoria,
            label=request.data["label"])
        messages.info(request, "Producto creado correctamente.")
        return Response(data=producto.pk)

    def update(self, request, pk=None):
        categoria = Categoria.objects.get(pk=request.POST["categoria"])
        post_imagen = request.FILES.get('imagen', None)

        print(request.POST)
        producto_actualizado = Producto.objects.get(pk=request.data["id_producto"], )

        producto_actualizado.titulo_producto = request.data["titulo_producto"]
        producto_actualizado.descripcion_producto = request.data["descripcion_producto"]
        producto_actualizado.precio = request.data["precio"]
        producto_actualizado.precio_descuento = request.data["precio_descuento"]
        producto_actualizado.stock = request.data["stock"]
        producto_actualizado.categoria = categoria
        producto_actualizado.label = request.data["label"]
        producto_actualizado.save()

        messages.info(request, "Producto actualizado correctamente.")
        return Response(data=producto_actualizado.pk)

    def destroy(self, request, pk):
        producto = Producto.objects.get(pk=pk)
        producto.delete()
        messages.danger(request, "Producto eliminado.")
        return Response("Producto eliminado")


class OrdenViewSet(viewsets.ModelViewSet):
    queryset = Orden.objects.all()
    serializer_class = OrdenSerializer

    def create(self, request, pk=None):
        user = request.data["user"]
        usuario = User.objects.get(username=user)
        precio_total = request.data["precio_total"]
        subtotal = request.data["subtotal"]
        IVA_venta = request.data["IVA_venta"]
        venta = Orden.objects.create(
            user=usuario, precio_total=precio_total, subtotal=subtotal, IVA_venta=IVA_venta)
        return Response(data=venta.pk)

    def get(self, request, pk=None):
        queryset = Orden.objects.all()
        venta = get_object_or_404(queryset, pk=pk)
        serializer = OrdenSerializer(venta)
        return Response(serializer.data)


class ProductoOrdenViewSet(viewsets.ModelViewSet):
    queryset = ProductoOrden.objects.select_related()
    serializer_class = ProductoOrdenSerializer

    def create(self, request):
        producto_id = request.data["producto"]
        producto = Producto.objects.get(pk=producto_id)
        venta_id = request.data["venta"]
        venta = Orden.objects.get(pk=venta_id)
        precio = request.data["precio"]
        cantidad = request.data["cantidad"]

        total_detalle = float(precio) * float(cantidad)
        venta = ProductoOrden.objects.create(producto=producto, venta=venta, precio=precio, cantidad=cantidad,
                                             total_detalle=total_detalle)
        return Response(data=venta.pk)


class ListaProductos(ListView):
    template_name = 'productos.html'
    model = Producto
    success_url = '/productos/'

    def get_queryset(self):
        data = self.model.objects

    def get_context_data(self, **kwargs):
        context = super(ListaProductos, self).get_context_data(**kwargs)
        context['productos'] = Producto.objects.all()
        context['categorias'] = Categoria.objects.all()
        context['labels'] = LABEL_CHOICES
        return context


# <===== VISTAS TEMPLATE =====>

@login_required(login_url='log_in')
def index(request):
    if request.user is not None:
        return render(request, 'base.html', {})
    else:
        return render(request, 'login.html', {})


@login_required(login_url='log_in')
@staff_member_required(login_url='catalogo')
def productos(request):
    if request.user is not None:
        categorias = Categoria.objects.all()
        return render(request, 'productos.html', {'categorias': categorias})
    else:
        return render(request, 'login.html', {})


def login(request):
    return render(request, 'login.html', {})


@login_required(login_url='log_in')
def punto_de_venta(request):
    listado_productos = Producto.objects.all()
    return render(request, 'punto_de_venta.html',
                  {'Usuario': request.user, 'listado_productos': listado_productos})


@login_required(login_url='log_in')
def historial_ventas(request):
    return render(request, 'historial_ventas.html',
                  {'Usuario': request.user})


class HomeView(FilterView, LoginRequiredMixin):
    model = Producto
    paginate_by = 10
    template_name = "catalogo.html"
    filterset_class = ProductFilter


class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Orden.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'order_summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")


class ItemDetailView(DetailView):
    model = Producto
    template_name = "producto.html"


@login_required(login_url='log_in')
def add_to_cart(request, slug):
    producto = get_object_or_404(Producto, slug=slug)
    order_item, created = ProductoOrden.objects.get_or_create(
        producto=producto,
        user=request.user,
        ordered=False
    )
    order_qs = Orden.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.productos.filter(producto__slug=producto.slug).exists():
            order_item.cantidad += 1
            order_item.save()
            messages.info(request, "La cantidad de productos fue actualizada.")
            return redirect("order-summary")
        else:
            order.productos.add(order_item)
            messages.info(request, "Este producto se ha añadido a su cesta.")
            return redirect("order-summary")
    else:
        ordered_date = timezone.now()
        order = Orden.objects.create(
            user=request.user, fecha_orden=ordered_date)
        order.productos.add(order_item)
        messages.info(request, "Este producto se ha añadido a su cesta.")
        return redirect("order-summary")


@login_required
def remove_from_cart(request, slug):
    producto = get_object_or_404(Producto, slug=slug)
    order_qs = Orden.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.productos.filter(producto__slug=producto.slug).exists():
            order_item = ProductoOrden.objects.filter(
                producto=producto,
                user=request.user,
                ordered=False
            )[0]
            order.productos.remove(order_item)
            order_item.delete()
            messages.info(request, "Este producto ha sido eliminado de su cesta.")
            return redirect("order-summary")
        else:
            messages.info(request, "Este artículo no estaba en su cesta.")
            return redirect("producto", slug=slug)
    else:
        messages.info(request, "No tiene ningún pedido activo")
        return redirect("producto", slug=slug)


@login_required
def remove_single_item_from_cart(request, slug):
    producto = get_object_or_404(Producto, slug=slug)
    order_qs = Orden.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.productos.filter(producto__slug=producto.slug).exists():
            order_item = ProductoOrden.objects.filter(
                producto=producto,
                user=request.user,
                ordered=False
            )[0]
            if order_item.cantidad > 1:
                order_item.cantidad -= 1
                order_item.save()
            else:
                order.productos.remove(order_item)
            messages.info(request, "La cantidad de productos fue actualizada.")
            return redirect("order-summary")
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("product", slug=slug)
    else:
        messages.info(request, "No tiene ningún pedido activo")
        return redirect("product", slug=slug)


class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            orden = Orden.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': orden
            }
            return render(self.request, 'resumen_orden.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "No tiene ningún pedido activo")
            return redirect("/")


class CheckoutView(View):
    def get(self, *args, **kwargs):
        try:
            orden = Orden.objects.get(user=self.request.user, ordered=False)
            form = CheckoutForm()
            context = {
                'form': form,
                #'couponform': CouponForm(),
                'orden': orden,
                'DISPLAY_COUPON_FORM': False
            }

            shipping_address_qs = Address.objects.filter(
                user=self.request.user,
                address_type='S',
                default=True
            )
            if shipping_address_qs.exists():
                context.update(
                    {'default_shipping_address': shipping_address_qs[0]})

            billing_address_qs = Address.objects.filter(
                user=self.request.user,
                address_type='B',
                default=True
            )
            if billing_address_qs.exists():
                context.update(
                    {'default_billing_address': billing_address_qs[0]})
            return render(self.request, "checkout.html", context)
        except ObjectDoesNotExist:
            messages.info(self.request, "You do not have an active order")
            return redirect("core:checkout")

    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            if form.is_valid():

                use_default_shipping = form.cleaned_data.get(
                    'use_default_shipping')
                if use_default_shipping:
                    print("Using the defualt shipping address")
                    address_qs = Address.objects.filter(
                        user=self.request.user,
                        address_type='S',
                        default=True
                    )
                    if address_qs.exists():
                        shipping_address = address_qs[0]
                        order.shipping_address = shipping_address
                        order.save()
                    else:
                        messages.info(
                            self.request, "No default shipping address available")
                        return redirect('core:checkout')
                else:
                    print("User is entering a new shipping address")
                    shipping_address1 = form.cleaned_data.get(
                        'shipping_address')
                    shipping_address2 = form.cleaned_data.get(
                        'shipping_address2')
                    shipping_country = form.cleaned_data.get(
                        'shipping_country')
                    shipping_zip = form.cleaned_data.get('shipping_zip')

                    if is_valid_form([shipping_address1, shipping_country, shipping_zip]):
                        shipping_address = Address(
                            user=self.request.user,
                            street_address=shipping_address1,
                            apartment_address=shipping_address2,
                            country=shipping_country,
                            zip=shipping_zip,
                            address_type='S'
                        )
                        shipping_address.save()

                        order.shipping_address = shipping_address
                        order.save()

                        set_default_shipping = form.cleaned_data.get(
                            'set_default_shipping')
                        if set_default_shipping:
                            shipping_address.default = True
                            shipping_address.save()

                    else:
                        messages.info(
                            self.request, "Please fill in the required shipping address fields")

                use_default_billing = form.cleaned_data.get(
                    'use_default_billing')
                same_billing_address = form.cleaned_data.get(
                    'same_billing_address')

                if same_billing_address:
                    billing_address = shipping_address
                    billing_address.pk = None
                    billing_address.save()
                    billing_address.address_type = 'B'
                    billing_address.save()
                    order.billing_address = billing_address
                    order.save()

                elif use_default_billing:
                    print("Using the defualt billing address")
                    address_qs = Address.objects.filter(
                        user=self.request.user,
                        address_type='B',
                        default=True
                    )
                    if address_qs.exists():
                        billing_address = address_qs[0]
                        order.billing_address = billing_address
                        order.save()
                    else:
                        messages.info(
                            self.request, "No default billing address available")
                        return redirect('core:checkout')
                else:
                    print("User is entering a new billing address")
                    billing_address1 = form.cleaned_data.get(
                        'billing_address')
                    billing_address2 = form.cleaned_data.get(
                        'billing_address2')
                    billing_country = form.cleaned_data.get(
                        'billing_country')
                    billing_zip = form.cleaned_data.get('billing_zip')

                    if is_valid_form([billing_address1, billing_country, billing_zip]):
                        billing_address = Address(
                            user=self.request.user,
                            street_address=billing_address1,
                            apartment_address=billing_address2,
                            country=billing_country,
                            zip=billing_zip,
                            address_type='B'
                        )
                        billing_address.save()

                        order.billing_address = billing_address
                        order.save()

                        set_default_billing = form.cleaned_data.get(
                            'set_default_billing')
                        if set_default_billing:
                            billing_address.default = True
                            billing_address.save()

                    else:
                        messages.info(
                            self.request, "Please fill in the required billing address fields")

                payment_option = form.cleaned_data.get('payment_option')

                if payment_option == 'S':
                    return redirect('core:payment', payment_option='stripe')
                elif payment_option == 'P':
                    return redirect('core:payment', payment_option='paypal')
                else:
                    messages.warning(
                        self.request, "Invalid payment option selected")
                    return redirect('core:checkout')
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("core:order-summary")
