from django.conf import settings
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include
from django.urls import path
from rest_framework import routers

from .views import views
from .views.custom_auth import (
    LogInView, SignUpView, LogOutView,
)
from .views.views import add_to_cart, remove_from_cart, OrderSummaryView, remove_single_item_from_cart, CheckoutView

router = routers.DefaultRouter()
router.register(r'categorias', views.CategoriaViewSet)
router.register(r'usuarios', views.UserViewSet)
router.register(r'productos', views.ProductoViewSet)
router.register(r'ventas', views.OrdenViewSet)
router.register(r'producto_ventas', views.ProductoOrdenViewSet)

urlpatterns = [
    # Index
    path('', views.HomeView.as_view(), name="index"),
    # Login
    path('log-in/', LogInView.as_view(), name='log_in'),
    path('log-out/', LogOutView.as_view(), name='log_out'),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('producto/<slug>/', views.ItemDetailView.as_view(), name='producto'),
    path('add-to-cart/<slug>/', add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<slug>/', remove_from_cart, name='remove-from-cart'),
    path('remove-item-from-cart/<slug>/', remove_single_item_from_cart,
         name='remove-single-item-from-cart'),
    path('resumen_orden/', OrderSummaryView.as_view(), name='order-summary'),
    path('checkout/', CheckoutView.as_view(), name='checkout'),
    path('sign_up/', SignUpView.as_view(), name='sign_up'),
    path('punto_de_venta', views.punto_de_venta, name="punto_de_venta", ),
    path('historial_ventas', views.historial_ventas, name="historial_ventas", ),
    path('productos', views.ListaProductos.as_view(), name="productos"),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
    path('', include('pwa.urls')),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
