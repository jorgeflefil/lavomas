from django import template
from App.models import Orden

register = template.Library()


@register.filter
def cart_item_count(user):
    if user.is_authenticated:
        qs = Orden.objects.filter(user=user, ordered=False)
        if qs.exists():
            return qs[0].productos.count()
    return 0