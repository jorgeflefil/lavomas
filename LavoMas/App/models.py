from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils.text import slugify
from djmoney.models.fields import MoneyField

LABEL_CHOICES = (
    ('P', 'primary'),
    ('S', 'success'),
    ('D', 'danger')
)

ADDRESS_CHOICES = (
    ('B', 'Boleta'),
    ('F', 'Factura'),
)


# Create your models here.

class Activation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=20, unique=True)
    email = models.EmailField(blank=True)


class Categoria(models.Model):
    nombre_categoria = models.CharField(max_length=40)

    def __str__(self):
        return self.nombre_categoria

    class Meta:
        verbose_name_plural = "Categorias"


class Producto(models.Model):
    titulo_producto = models.CharField(max_length=100)
    descripcion_producto = models.TextField()
    precio = MoneyField(max_digits=6, decimal_places=0, default_currency='CLP')
    precio_descuento = MoneyField(max_digits=6, decimal_places=0, default_currency='CLP', blank=True, null=True, default=None)
    stock = models.IntegerField(default=0)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    label = models.CharField(choices=LABEL_CHOICES, max_length=1)
    slug = models.SlugField()
    imagen_producto = models.ImageField(upload_to='', blank=True, null=True)

    def get_absolute_url(self):
        return reverse("producto", kwargs={
            'slug': self.slug
        })

    def get_add_to_cart_url(self):
        return reverse("add-to-cart", kwargs={
            'slug': self.slug
        })

    def get_remove_from_cart_url(self):
        return reverse("remove-from-cart", kwargs={
            'slug': self.slug
        })

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo_producto)
        super(Producto, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Productos"

    def __str__(self):
        return self.titulo_producto


class ProductoOrden(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.cantidad} of {self.producto.titulo_producto}"

    def get_total_item_price(self):
        return self.cantidad * self.producto.precio

    def get_total_discount_item_price(self):
        return self.cantidad * self.producto.precio_descuento

    def get_amount_saved(self):
        return self.get_total_item_price() - self.get_total_discount_item_price()

    def get_final_price(self):
        if self.producto.precio_descuento:
            return self.get_total_discount_item_price()
        return self.get_total_item_price()

    class Meta:
        verbose_name_plural = "Productos ordenes"


class Orden(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE)
    productos = models.ManyToManyField(ProductoOrden)
    precio_total = models.DecimalField(max_digits=10,
                                       decimal_places=2,
                                       default=0)
    subtotal = models.DecimalField(max_digits=10,
                                   decimal_places=2,
                                   default=0)
    IVA_venta = models.DecimalField(max_digits=10,
                                    decimal_places=2,
                                    default=0)
    fecha_orden = models.DateTimeField(auto_now=True)
    ordered = models.BooleanField(default=False)
    shipping_address = models.ForeignKey(
        'Address', related_name='shipping_address', on_delete=models.SET_NULL, blank=True, null=True)
    billing_address = models.ForeignKey(
        'Address', related_name='billing_address', on_delete=models.SET_NULL, blank=True, null=True)

    payment = models.ForeignKey(
        'Payment', on_delete=models.SET_NULL, blank=True, null=True)
    being_delivered = models.BooleanField(default=False)
    received = models.BooleanField(default=False)

    def get_total(self):
        total = 0
        for order_item in self.productos.all():
            total += order_item.get_final_price()
        return total

    class Meta:
        verbose_name_plural = "Ordenes"

    def __str__(self):
        return str(self.pk)


class Address(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    street_address = models.CharField(max_length=100)
    apartment_address = models.CharField(max_length=100)
    address_type = models.CharField(max_length=1, choices=ADDRESS_CHOICES)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Addresses'


class Payment(models.Model):
    stripe_charge_id = models.CharField(max_length=50)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.SET_NULL, blank=True, null=True)
    amount = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username