from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Categoria, Producto, Orden, ProductoOrden


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ('url', 'id', 'nombre_categoria')


class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = (
            'url', 'pk', 'titulo_producto', 'descripcion_producto', 'precio', 'precio_descuento', 'stock', 'categoria',
            'label', 'imagen_producto',)
        datatables_always_serialize = (
            'pk', 'titulo_producto', 'descripcion_producto', 'precio', 'precio_descuento', 'stock', 'categoria',
            'label', 'imagen_producto')


class ProductoImgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ('imagen_producto',)


class ProductoOrdenSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    nombre_producto = serializers.CharField(
        source='producto.descripcion_producto', read_only=True)

    class Meta:
        model = ProductoOrden
        fields = ('id', 'nombre_producto', 'producto', 'venta',
                  'precio', 'cantidad', 'total_detalle')
        datatables_always_serialize = ('id',)


class OrdenSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    fecha = serializers.DateTimeField(format="%H:%M:%S %d-%m-%Y")
    ventas = ProductoOrdenSerializer(many=True, read_only=True)

    class Meta:
        model = Orden
        fields = ('url', 'id', 'subtotal', 'IVA_venta',
                  'fecha', 'precio_total', 'ventas',)
        datatables_always_serialize = ('id')
